// ----------------------------------------------------------------------------

'use strict'

// core
const http = require('http')

// npm
const log = require('logfmtr').default()
const ms = require('ms')

// local
const pkg = require('./package.json')
const env = require('./lib/env.js')
const app = require('./lib/app.js')

// ----------------------------------------------------------------------------
// setup

process.title = pkg.name

log.info('started')

if (env.isDev) {
  log.info(env, 'env')
}

// every so often, print memory usage
const memUsageEveryMs = env.isProd ? ms('10 mins') : ms('30s')
setInterval(() => {
  log.withFields(process.memoryUsage()).debug('memory')
}, memUsageEveryMs)

// ----------------------------------------------------------------------------
// server

const server = http.createServer()
server.on('request', app)

server.listen(env.port, () => {
  log.withFields({ port: env.port }).info('server-started')
})

// ----------------------------------------------------------------------------
// cleanup

function cleanup (callback) {
  setTimeout(callback, 100)
}

// we'll get this from nodemon in development
process.once('SIGUSR2', () => {
  log.info('SIGUSR2')
  cleanup((err) => {
    console.log('Finished')
    if (err) {
      console.warn(err)
    }
    process.kill(process.pid, 'SIGUSR2')
  })
})

process.on('SIGTERM', () => {
  log.info('SIGTERM')
  cleanup((err) => {
    console.log('Finished')
    if (err) {
      console.warn(err)
    }
    process.exit(err ? 2 : 0)
  })
})

process.on('SIGINT', () => {
  log.info('SIGINT')
  cleanup((err) => {
    console.log('Finished')
    if (err) {
      console.warn(err)
    }
    process.exit(err ? 2 : 0)
  })
})

// ----------------------------------------------------------------------------
