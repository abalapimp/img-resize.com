// ----------------------------------------------------------------------------

// core
const path = require('path')

// npm
const express = require('express')
const compression = require('compression')
const LogFmtr = require('logfmtr')
const favicon = require('serve-favicon')
const multer = require('multer')
const moment = require('moment')
const yid = require('yid')
const errorHandler = require('errorhandler')
const ms = require('ms')

// local
const pkg = require('../package.json')
const env = require('./env.js')
const constants = require('./constants.js')
const stats = require('./stats.js')
const resize = require('./resize.js')

// ----------------------------------------------------------------------------
// setup

const log = LogFmtr.default()

const viewsDir = path.join(__dirname, '..', 'views')
const publicDir = path.join(__dirname, '..', 'public')
const faviconFilename = path.join(publicDir, 'favicon.ico')

const upload = multer({
  dest: '/tmp/' + pkg.name,
  limits: {
    fileSize: 5 * 1024 * 1024 // 5MB
  }
})

// create the sitemap
const sitemap = [
  `${env.baseUrl}/`,
  `${env.baseUrl}/examples`
]
const sitemapTxt = sitemap.join('\n') + '\n'

// --------------------------------------------------------------------------------------------------------------------
// app

const app = express()

app.set('views', viewsDir)
app.set('view engine', 'pug')
app.enable('trust proxy')

app.locals.pkg = pkg
app.locals.env = env
app.locals.min = env.isProd ? '.min' : ''

// static routes
app.use(compression())
app.use(favicon(faviconFilename))

app.use(express.static(publicDir, { maxAge: env.isProd ? ms('1 month') : ms('10s') }))

app.use(upload.single('input'))

// ----------------------------------------------------------------------------
// middleware

app.use((req, res, next) => {
  // add a Request ID
  req._rid = yid()

  // create a RequestID and set it on the `req.log`
  req.log = log.withFields({ rid: req._rid })

  next()
})

app.use(LogFmtr.middleware)

app.use((req, res, next) => {
  // set a `X-Made-By` header :)
  res.setHeader('X-Made-By', 'Andrew Chilton - https://chilts.org - @andychilton')

  // From: http://blog.netdna.com/opensource/bootstrapcdn/accept-encoding-its-vary-important/
  res.setHeader('Vary', 'Accept-Encoding')

  res.locals.title = false
  res.locals.moment = moment

  // add the advert
  res.locals.ad = {
    title: 'Digital Ocean',
    url: 'https://www.digitalocean.com/?refcode=c151de638f83',
    src: 'https://s18.postimg.org/zbi91biah/digital-ocean-728x90.jpg',
    text1: 'We recommend ',
    text2: ' for hosting your sites. Free $10 credit when you sign up.'
  }

  next()
})

// ----------------------------------------------------------------------------
// routes

app.get(
  '/',
  (req, res) => {
    stats.home.inc()
    res.render('index', { title: 'Image Resize' })
  }
)

app.get(
  '/examples',
  (req, res) => {
    stats.examples.inc()
    res.render('examples', { title: 'Language Examples for Image Resize' })
  }
)

app.post('/resize', (req, res, next) => {
  req.log.info('/resize : resizing input')

  // if there is no files at all, tell the user they need to upload one
  console.log('-------------------------------------------------------------------------------')
  console.log('req.file:', req.file)
  console.log('-------------------------------------------------------------------------------')
  if (!req.file) {
    req.log.warn('no-req.file')
    res.setHeader('Content-Type', 'text/plain')
    return res.status(500).send('Provide an input image.')
  }

  const file = req.file

  // now for the body
  if (!(req.body.op in resize)) {
    return res.status(400).send('Unknown operation : ' + (req.body.op || '(none)'))
  }

  if (!(file.mimetype in constants.extensions)) {
    req.log.warn('File type is not allowed, try a PNG, JPG, or WEBP.')
    res.setHeader('Content-Type', 'text/plain')
    return errorHandler(new Error('File type is not allowed, try a PNG, JPG, or WEBP.'))
  }

  function handler (err, info) {
    if (err) return errorHandler(err)
    resizeHandler(info)
  }

  // setup an error handler so we don't have to repeat ourselves
  function errorHandler (err) {
    console.log('INSIDE errorHandler()')
    if (req.xhr) {
      // return a JSON object
      return res.json({ ok: false, msg: '' + err })
    }

    // not an xhr request
    res.setHeader('Content-Type', 'text/plain')
    res.status(500).send('' + err)
  }

  function resizeHandler (info) {
    console.log('INSIDE resizeHandler() : ', info)
    // resize ok, now let's either send back JSON or the file itself
    if (req.xhr) {
      return res.json(info)
    }

    // a regular post from a form or command line
    var outFilename = constants.outDir + '/' + file.filename + '.' + constants.extensions[file.mimetype]
    res.download(outFilename, 'image.' + constants.extensions[file.mimetype], (err) => {
      if (err) {
        req.log.error('Something went wrong when calling res.download(): ' + err)
        return next(err)
      }
      req.log.info('res-sendfile-complete')
    })
  }

  // do the operation requested
  var op = req.body.op

  // ok, let's do a resize to a fixed width
  if (op === 'fixedWidth') {
    resize.fixedWidth(file, req.body.width, handler)
  } else if (op === 'fixedHeight') {
    resize.fixedHeight(file, req.body.height, handler)
  } else if (op === 'scale') {
    resize.scale(file, req.body.scaledWidth, req.body.scaledHeight, handler)
  } else if (op === 'stretch') {
    resize.stretch(file, req.body.stretchedWidth, req.body.stretchedHeight, handler)
  } else if (op === 'letterbox') {
    resize.letterbox(file, req.body.letterboxWidth, req.body.letterboxHeight, req.body.letterboxColor, handler)
  } else if (op === 'percentage') {
    resize.percentage(file, req.body.percent, handler)
  } else {
    return errorHandler(new Error('Unknown operation : ' + op))
  }

  // finally, increment the stats
  stats.resize.inc()
})

app.get('/dl/:filename', (req, res, next) => {
  var outFilename = constants.outDir + '/' + req.params.filename
  res.download(outFilename, req.params.filename, (err) => {
    if (err) {
      req.log.error('Something went wrong when calling res.download(): ' + err)
      return next(err)
    }
    stats.download.inc()
    req.log.info('res-download-complete')
  })
})

app.use(
  '/view/',
  (req, res, next) => {
    stats.view.inc()
    next()
  },
  express.static(constants.outDir)
)

app.get(
  '/stats',
  (req, res, next) => {
    var finished = false
    var got = 0
    var currentStats = {}

    // get some bits
    stats.pages.forEach((hitName) => {
      stats[hitName].values((err, data) => {
        if (finished) return
        if (err) {
          finished = true
          return next(err)
        }

        got += 1

        // save this hit
        data.forEach((hit) => {
          currentStats[hit.ts] = currentStats[hit.ts] || {}
          currentStats[hit.ts][hitName] = hit.val
        })

        // if we've got all the results, render the page
        if (got === stats.pages.length) {
          finished = true
          res.render('stats', { stats: currentStats, title: 'stats' })
        }
      })
    })
  }
)

app.get(
  '/sitemap.txt',
  (req, res) => {
    res.setHeader('Content-Type', 'text/plain')
    res.send(sitemapTxt)
  }
)

app.use((err, req, res, next) => {
  if (err.code === 'LIMIT_FILE_SIZE') {
    var data = {
      ok: false,
      msg: 'File is too big, should be less than 5MB.'
    }
    return res.json(data)
  }
  next(err)
})

// error handlers
if (env.isDev) {
  app.use(errorHandler({ dumpExceptions: true, showStack: true }))
}

// ----------------------------------------------------------------------------

module.exports = app

// ----------------------------------------------------------------------------
