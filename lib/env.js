// ----------------------------------------------------------------------------

const isProd = process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'staging'
const isDev = !isProd

const port = process.env.PORT || 3000
const apex = process.env.APEX
if (!apex) {
  throw new Error('No APEX env var defined')
}
const baseUrl = isProd ? `https://${apex}` : `http://${apex}`

const carbonAdsServe = process.env.CARBON_ADS_SERVE
const carbonAdsPlacement = process.env.CARBON_ADS_PLACEMENT

const googleAnalytics = process.env.GOOGLE_ANALYTICS

// ----------------------------------------------------------------------------

module.exports = {
  isProd,
  isDev,
  port,
  apex,
  baseUrl,
  carbonAdsServe,
  carbonAdsPlacement,
  googleAnalytics
}

// ----------------------------------------------------------------------------
