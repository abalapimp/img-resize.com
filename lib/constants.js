// ----------------------------------------------------------------------------

// local
const pkg = require('../package.json')

// ----------------------------------------------------------------------------

// directories
module.exports.outDir = '/var/lib/' + pkg.name + '/'

module.exports.extensions = {
  'image/png': 'png',
  'image/jpg': 'jpg',
  'image/jpeg': 'jpg',
  'image/webp': 'webp'
}

// ----------------------------------------------------------------------------
