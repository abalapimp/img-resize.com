// ----------------------------------------------------------------------------

// npm
const sharp = require('sharp')

// local
const constants = require('./constants.js')

// ----------------------------------------------------------------------------

// # Example File #
//
// ```
// file: {
//   fieldname: 'input',
//   originalname: '20161112-141911a.jpg',
//   encoding: '7bit',
//   mimetype: 'image/jpeg',
//   destination: '/tmp/img-resize.com',
//   filename: 'a2a5040a79d356b9a5517324bc074b7f',
//   path: '/tmp/img-resize.com/a2a5040a79d356b9a5517324bc074b7f',
//   size: 4088126
// }
// ```
//
// (Ends)

// helpers

function imageToFileAndCallback (image, file, origSize, callback) {
  image.toFile(constants.outDir + '/' + file.filename + '.' + constants.extensions[file.mimetype], (err, info) => {
    if (err) return callback(err)
    callback(null, {
      ok: true,
      filename: file.filename,
      view: '/view/' + file.filename + '.' + constants.extensions[file.mimetype],
      download: '/dl/' + file.filename + '.' + constants.extensions[file.mimetype],
      origSize: origSize,
      newSize: { width: info.width, height: info.height }
    })
  })
}

// ----------------------------------------------------------------------------

function fixedWidth (file, width, callback) {
  // ToDo: check that we were given a file

  // convert to a number
  width = width | 0
  if (width < 0 || width > 10000) {
    // too big or small
    return process.nextTick(() => {
      callback(new Error('Invalid width, should be between 0 and 10,000'))
    })
  }

  var origSize = {
    width: 0,
    height: 0
  }
  const image = sharp(file.path)
  image
    .rotate()
    .metadata()
    .then(metadata => {
      console.log('info:', metadata)
      origSize = { width: metadata.width, height: metadata.height }
      console.log('origSize:', origSize)
      return image.resize(width, null)
    })
    .then(() => {
      imageToFileAndCallback(image, file, origSize, callback)
    })
}

function fixedHeight (file, height, callback) {
  // ToDo: check that we were given a file

  // convert to a number
  height = height | 0
  if (height < 0 || height > 10000) {
    // too big or small
    return process.nextTick(() => {
      callback(new Error('Invalid height, should be between 0 and 10,000'))
    })
  }

  var origSize = {
    width: 0,
    height: 0
  }
  const image = sharp(file.path)
  image
    .rotate()
    .metadata()
    .then(metadata => {
      console.log('info:', metadata)
      origSize = { width: metadata.width, height: metadata.height }
      console.log('origSize:', origSize)
      return image.resize(null, height)
    })
    .then(() => {
      imageToFileAndCallback(image, file, origSize, callback)
    })
}

function scale (file, width, height, callback) {
  // ToDo: check that we were given a file

  // convert to a number
  width = width | 0
  height = height | 0
  if (width < 0 || width > 10000) {
    // too big or small
    return process.nextTick(() => {
      callback(new Error('Invalid width, should be between 0 and 10,000'))
    })
  }
  if (height < 0 || height > 10000) {
    // too big or small
    return process.nextTick(() => {
      callback(new Error('Invalid height, should be between 0 and 10,000'))
    })
  }

  var origSize = {
    width: 0,
    height: 0
  }
  const image = sharp(file.path)
  image
    .rotate()
    .metadata()
    .then(metadata => {
      console.log('info:', metadata)
      origSize = { width: metadata.width, height: metadata.height }
      console.log('origSize:', origSize)
      return image.resize({ width, height, fit: sharp.fit.cover })
    })
    .then(() => {
      imageToFileAndCallback(image, file, origSize, callback)
    })
}

function stretch (file, width, height, callback) {
  // ToDo: check that we were given a file

  // convert to a number
  width = width | 0
  height = height | 0
  if (width < 0 || width > 10000) {
    // too big or small
    return process.nextTick(() => {
      callback(new Error('Invalid width, should be between 0 and 10,000'))
    })
  }
  if (height < 0 || height > 10000) {
    // too big or small
    return process.nextTick(() => {
      callback(new Error('Invalid height, should be between 0 and 10,000'))
    })
  }

  var origSize = {
    width: 0,
    height: 0
  }
  const image = sharp(file.path)
  image
    .rotate()
    .metadata()
    .then(metadata => {
      console.log('info:', metadata)
      origSize = { width: metadata.width, height: metadata.height }
      console.log('origSize:', origSize)
      return image.resize({ width, height, fit: sharp.fit.fill })
    })
    .then(() => {
      imageToFileAndCallback(image, file, origSize, callback)
    })
}

function letterbox (file, width, height, color, callback) {
  // ToDo: check that we were given a file

  // convert to a number
  width = width | 0
  height = height | 0
  if (width < 0 || width > 10000) {
    // too big or small
    return process.nextTick(() => {
      callback(new Error('Invalid width, should be between 0 and 10,000'))
    })
  }
  if (height < 0 || height > 10000) {
    // too big or small
    return process.nextTick(() => {
      callback(new Error('Invalid height, should be between 0 and 10,000'))
    })
  }
  color = color || '000000'
  var match = color.match(/^#?([0-9a-f]{2})([0-9a-f]{2})([0-9a-f]{2})$/)
  if (!match) {
    // too big or small
    return process.nextTick(() => {
      callback(new Error('Invalid color, should be a hex code of 6 chars long, e.g. ff0000.'))
    })
  }
  const rgb = {
    r: parseInt(match[1], 16),
    g: parseInt(match[2], 16),
    b: parseInt(match[3], 16)
  }
  console.log('rgb:', rgb)

  var origSize = {
    width: 0,
    height: 0
  }
  const image = sharp(file.path)
  image
    .rotate()
    .metadata()
    .then(metadata => {
      origSize = { width: metadata.width, height: metadata.height }
      return image.resize({ width, height, background: rgb, fit: sharp.fit.contain })
    })
    .then(() => {
      imageToFileAndCallback(image, file, origSize, callback)
    })
}

function percentage (file, percent, callback) {
  // ToDo: check that we were given a file

  // convert to a number
  percent = percent | 0
  if (percent < 0 || percent > 100) {
    // too big or small
    return process.nextTick(() => {
      callback(new Error('Invalid percent, should be between 0 and 100'))
    })
  }

  var origSize = {
    width: 0,
    height: 0
  }
  const image = sharp(file.path)
  image
    .rotate()
    .metadata()
    .then(metadata => {
      console.log('info:', metadata)
      origSize = { width: metadata.width, height: metadata.height }
      console.log('origSize:', origSize)
      return image.resize(Math.round(metadata.width * percent / 100))
    })
    .then(() => {
      imageToFileAndCallback(image, file, origSize, callback)
    })
}

// --------------------------------------------------------------------------------------------------------------------

module.exports = {
  fixedWidth,
  fixedHeight,
  scale,
  stretch,
  letterbox,
  percentage
}

// ----------------------------------------------------------------------------
