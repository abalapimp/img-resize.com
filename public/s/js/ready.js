// Thanks to Shiv Kumar for this simple example of HTML5 upload with progress.
// From: http://www.matlus.com/html5-file-upload-with-progress/

/* eslint-env browser, jquery */

$(function () {
  // functions
  function setState () {
    // Disable most elements in the form
    $('#input-form table input[type="text"]').prop('disabled', true)

    // enable any related to the 'checked' operation (if any)
    var $checked = $('input:radio[name=op]:checked')
    $checked.parents('tr').first().find('input').prop('disabled', false)
  }

  // when the file select changes, show the details on the right
  $('#file').change(function (ev) {
    ev.preventDefault()

    // get this input
    var file = this.files[0]

    current = {
      filename: file.name,
      size: file.size,
      type: file.type,
      modified: file.lastModifiedDate
    }

    $('#filename').text(file.name)
    $('#size').text(file.size)
    $('#filetype').text(file.type)
    $('#modified').text(file.lastModifiedDate)
  })

  // when a radio button is clicked, re-do the states
  $('input:radio[name=op]').click(function (ev) {
    setState()
  })

  // set the initial state of the page
  setState()

  // if FormData and XHR2 are not available, we'll just do a regular form post
  if (!window.FormData) { return }

  // keep a count of the number of uploads (and therefore can use as an id)
  var id = 1

  // save some elements
  var $uploads = $('#uploads')

  function getUploadRow (id) {
    return $uploads.find('tr').filter(function (i) {
      var $this = $(this)
      return $this.data('id') === id
    })
  }

  // the currently selected file
  var current = {}

  // set up some of the
  function uploadProgress (evt, id) {
    if (evt.lengthComputable) {
      var percentComplete = Math.round(evt.loaded * 100 / evt.total)
      getUploadRow(id).find('.status').text(percentComplete + '%')
    } else {
      $('#error').text('unable to compute')
    }
  }

  function uploadComplete (evt, id) {
    // This event is raised when the server sent back a response
    var data = JSON.parse(evt.target.responseText)
    console.log('data:', data)

    if (data.ok) {
      var $row = getUploadRow(id)
      $row.find('.status').text('Image resized.')

      // set the link
      var $a1 = $('<a target="_blank">').attr('href', data.view).text('View')
      var $output1 = $row.find('.view')
      $output1.append($a1)
      var $a2 = $('<a>').attr('href', data.download).text('Download')
      var $output2 = $row.find('.download')
      $output2.append($a2)

      var $filename = $row.find('.filename')
      $filename.text(data.filename)
      var $orig = $row.find('.origSize')
      $orig.text('(' + data.origSize.width + 'x' + data.origSize.height + ')')
      var $new = $row.find('.newSize')
      $new.text('(' + data.newSize.width + 'x' + data.newSize.height + ')')
    } else {
      getUploadRow(id).find('.status').text(data.msg)
    }
  }

  function uploadFailed (evt, id) {
    getUploadRow(id).find('.status').text('There was an error attempting to upload the file.')
  }

  function uploadAborted (evt, id) {
    getUploadRow(id).find('.status').text('The upload has been aborted by the user or the browser dropped the connection.')
  }

  function uploadFile () {
    // set an id for this upload
    var thisId = id++

    // add a new row to the $uploads table
    var $html = $('<tr><td class="filename"></td><td class="origSize"></td><td class="status"></td><td class="newSize"></td><td class="view"></td><td class="download"></td></tr>')
    $html.data('id', thisId)
    $uploads.prepend($html)

    $html.find('td.filename').text($('#filename').text())

    // set some things on this new row
    $html.find('td.filename').text(current.filename)
    $html.find('td.old').text(current.size)
    $html.find('td.progress').text('0%')

    // Info: http://caniuse.com/xhr2
    var xhr = new XMLHttpRequest()

    // make a new FormData from the form itself
    var fd = new FormData($('#input-form')[0])

    /* event listners */
    function wrap (fn) {
      return function (evt) {
        fn(evt, thisId)
      }
    }
    xhr.upload.addEventListener('progress', wrap(uploadProgress), false)
    xhr.addEventListener('load', wrap(uploadComplete), false)
    xhr.addEventListener('error', wrap(uploadFailed), false)
    xhr.addEventListener('abort', wrap(uploadAborted), false)

    // and finally, POST to /resize
    xhr.open('POST', '/resize')

    // like jQuery, we should set this so Express knows and can tell us it's an XHR
    xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest')

    // ... and send it
    xhr.send(fd)
  }

  $('#resize').click(function (ev) {
    ev.preventDefault()
    uploadFile()
  })

  $('#clear').click(function (ev) {
    $('#filename, #size, #filetype, #modified').text('')
    $uploads.empty()
  })
})
