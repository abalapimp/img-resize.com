all:
	echo 'Try: make test'

server:
	NODE_ENV=development supervisor --no-restart-on error -- server.js

test:
	curl -X POST -s --form 'op=fixedWidth' --form 'width=200' --form 'input=@flowers.jpg;type=image/jpg' http://localhost:8371/resize > flowers-200x150.jpg

clean:
	find . -name '*~' -delete

.PHONY: server test clean
